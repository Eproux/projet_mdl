<?php
if (isset($_REQUEST['action'])){ // Si il n'y a pas d'action renseigné, on redirige vers la connexion
    $action = $_REQUEST['action'];
}
switch($action){

    case 'gestdemande':
    {

        $lesdemandes = $pdo->GetDemande();
        include("vues/vue_ListeDemande.php");

        break;
    }

    case 'traitementDemande':
    {
        if(isset($_REQUEST['param'])) {

            if ($_REQUEST['param'] == "demandeTraiter") {
                if ($_REQUEST['choix'] == 0) {


                    $iddemande = $_REQUEST['iddemande'];

                    $laDemande = $pdo->GetRecupDemande($iddemande);

                    foreach ($laDemande as $info) {

                        $Id_M = $info['Id_M'];

                    }

                    $pdo->RefusDemande($iddemande);


                    $email= $pdo->GetEmailM($Id_M);

                    echo $email[0][0];

                    date_default_timezone_set("Europe/Paris");

                    $mail = new PHPMailer();

                    $body = utf8_decode("Bonjour, je vous informe que votre demande d'association a été rejeté car elle n'est pas conforme aux exigences demandé.</br></br> Cordialement,</br> Votre administrateur ");

                    $mail->isMail();
                    $mail->SMTPAuth = true;
                    $mail->Host = "localhost";
                    $mail->Port = 25;

                    //$mail->Username = "";
                    //$mail->Password = "";
                    $mail->From = "enzoo.gaming@gmail.com"; //adresse d’envoi correspondant au login entrée précédement
                    $mail->FromName = "Administrateur"; // nom qui sera affiché
                    $mail->Subject = utf8_decode("Réponse Demande Association"); // sujet
                    $mail->AltBody = $body; //Body au format texte

                    $mail->WordWrap = 50; // nombre de caractere pour le retour a la ligne automatique
                    $mail->MsgHTML($body);


                    $mail->AddAddress($email[0][0]);
                    $mail->IsHTML(true); // envoyer au format html, passer a false si en mode texte
                    $mail->Send();



                    $lesdemandes = $pdo->GetDemande();

                    include("vues/vue_ListeDemande.php");

                }else{
                    if ($_REQUEST['choix'] == 1) {

                        $iddemande = $_REQUEST['iddemande'];

                        $laDemande = $pdo->GetRecupDemande($iddemande);

                        foreach ($laDemande as $info) {

                            $nomAssociation = $info['nomAssociation'];
                            $nomFondateur = $info['nomFondateur'];
                            $tel = $info['tel'];
                            $mail = $info['mail'];
                            $description = $info['description'];
                            $categorie = $info['categorie'];
                            $etat = $info['etat'];
                            $Id_M = $info['Id_M'];

                        }


                        $iddemande = $_REQUEST['iddemande'];
                        $NbrAssoc = $pdo->GetNbrAssoc();
                        $id = $NbrAssoc[0] + 1;

                        $Cat = $pdo->getRecupCat($categorie);
                        $cate = $Cat[0];

                        if($Cat == false){

                            $idC=$pdo->SetCate($categorie);


                            $pdo->AcceptDemande($idC, $id, $description, $mail, $tel, $nomAssociation, $Id_M);

                        }else{

                            $pdo->AcceptDemande($cate, $id, $description, $mail, $tel, $nomAssociation, $Id_M);
                        }


                        $pdo->RefusDemande($iddemande);


                        $email= $pdo->GetEmailM($Id_M);

                        //echo $email[0][0];

                        date_default_timezone_set("Europe/Paris");

                        $mail = new PHPMailer();

                        $body = utf8_decode("Bonjour, je vous informe que votre demande d'association a été accepté. Vous pouvez vous connecter pour gérer votre association.</br></br> Cordialement,</br> Votre administrateur ");

                        $mail->isMail();
                        $mail->SMTPAuth = true;
                        $mail->Host = "localhost";
                        $mail->Port = 25;

                        //$mail->Username = "";
                      //  $mail->Password = "";
                        $mail->From = "enzoo.gaming@gmail.com"; //adresse d’envoi correspondant au login entrée précédement
                        $mail->FromName = "Administrateur"; // nom qui sera affiché
                        $mail->Subject = utf8_decode("Réponse Demande Association"); // sujet
                        $mail->AltBody = $body; //Body au format texte

                        $mail->WordWrap = 50; // nombre de caractere pour le retour a la ligne automatique
                        $mail->MsgHTML($body);


                        $mail->AddAddress($email[0][0]);
                        $mail->IsHTML(true); // envoyer au format html, passer a false si en mode texte
                        $mail->Send();

                        $lesdemandes = $pdo->GetDemande();

                       // $mail->ErrorInfo;

                       include("vues/vue_ListeDemande.php");



                    }
                }

            }

        }

        break;
    }


    case 'inscriAssoc':{

        $idassoc= $_REQUEST['idassoc'];
        $idMembre =  $_SESSION['id'];


        $pdo->InscriAssociation($idassoc,$idMembre);


        $message="Inscription Réussi";
        include("vues/vue_message.php");


        break ;

    }




}

?>