<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Thib'
 * Date: 13/09/13
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
class PdoPPE
{
    private static $serveur='mysql:host=localhost';
    //private static $serveur='mysql:host=172.18.207.102';
    private static $bdd='dbname=u319553517_mdl';
    private static $user='u319553517_enzo' ;
    private static $mdp='Enzo0632066458toul' ;
    private static $monPdo;
    private static $monPdoPPE = null;
    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct()
    {
        PdoPPE::$monPdo = new PDO(PdoPPE::$serveur.';'.PdoPPE::$bdd, PdoPPE::$user, PdoPPE::$mdp);
        PdoPPE::$monPdo->query("SET CHARACTER SET utf8");
    }
    public function _destruct(){
        PdoPPE::$monPdo = null;
    }
    /**
     * Fonction statique qui crée l'unique instance de la classe
     *
     */
    public  static function getPdoPPE()
    {
        if(PdoPPE::$monPdoPPE == null)
        {
            PdoPPE::$monPdoPPE= new PdoPPE();
        }
        return PdoPPE::$monPdoPPE;
    }
    /*
    * Test le couple login/mdp et retourne 0 si le couple n'existe pas et 1 si il existe
    * @Parametres : login et mdp
    */
    public function testLogMembre($l, $p)
    {
        $req = "select * from membres where Login = '".$l."' and Mdp ='".$p."'";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetch();
        // test echo var_dump($leResu);
        return $leResu;

    }
	
	public function getNomAssoc($id)
    {
        $req = "select Nom from associations where IdAssoc= $id";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetch();
     
        return $leResu;

    }
	

    public function testLogAdmin($l, $p)
    {
        $req = "select count(*) from administrateur where Login = '".$l."' and Password ='".$p."'";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetch();
        // test echo var_dump($leResu);
        return $leResu;

    }


    public function recupID($l,$p){


        $req2 = "select IdMembre from membres where Login = '".$l."' and Mdp = '".$p."'";
        $res2 = PdoPPE::$monPdo->query($req2);
        $leRes = $res2->fetch();

        return $leRes;

    }


    public function AssociationMembre($id){


        $req = "select associations.Nom, associations.IdAssoc from associations,contenir,membres where membres.IdMembre = contenir.IdMembre and  contenir.IdAssoc = associations.IdAssoc  and membres.IdMembre = $id ";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;

    }



    public function AssociationfondMembre($id){

        $req = "select * from associations where `IdMembre`=$id";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;

    }

    public function InscriAssociation($idassoc,$idMembre){

        $req = "INSERT INTO `contenir`(`IdAssoc`, `IdMembre`) VALUES ($idassoc, $idMembre)";
        $res = PdoPPE::$monPdo->exec($req);


    }


    public function getrecupCategorie()
    {


        $req = "select *  from categories";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();


        return $leResu;



    }

    public function getRecupAssocDuMembre($id){

        $req = "select * from associations,contenir where associations.IdAssoc = contenir.IdAssoc and contenir.IdMembre = $id";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;


    }

    public function getRechercheMembre($nom){

        $req = "select * from membres where NomMembre ='$nom' or Prenom ='$nom'";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;

    }

    public function ValiderCotisation($id,$idMembre,$montant,$date){

        $req = "INSERT INTO `cotisation`(`Montant`, `IdMembre`, `IdAssoc`, `date`) VALUES ('".$montant."',$idMembre,$id,'".$date."')";
        $res = PdoPPE::$monPdo->exec($req);

       
    }


    public function getAdmin($idMembre,$id){

        $req = "select * from associations where IdMembre = $idMembre and IdAssoc=$id ";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;


    }

    public function suppcotisation($idMembreSupp,$id){


        $req = "Delete from cotisation where IdAssoc= $id and IdMembre=$idMembreSupp";
        $res = PdoPPE::$monPdo->exec($req);

    }



    public function suppMembre($idMembreSupp,$id){

        $req = "Delete from contenir where IdAssoc= $id and IdMembre=$idMembreSupp";
        $res = PdoPPE::$monPdo->exec($req);
    }

    public function getCotisations($id){

        $req = "select NomMembre,Prenom,Montant,Date from cotisation,membres where cotisation.IdMembre = membres.IdMembre and cotisation.IdAssoc=$id";
        $res = PdoPPE::$monPdo->query($req);
        $leResu = $res->fetchAll();
        return $leResu;


    }




    public function getrecupAssociation()
    {

        $req = "select Nom,Fiche,IdAssoc from associations Group by Nom ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;

    }


    public function getrecupAssociationDeLaCat($id)
    {

        $req = "select Nom,Fiche,IdAssoc from associations where IdCategories= $id Group by Nom ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;


    }



    public function getMembreInscrit($id,$idMembre){

        $req = "select * from contenir where IdAssoc = $id and IdMembre=$idMembre ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetch();

        return $leRes;


    }

    public function getInfoAssociation($id)


    {


        $req = "select * from associations where IdAssoc = $id";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;



    }

    public function  ModifFiche($id,$fiche){

        $req = " UPDATE associations SET Fiche ='".$fiche."' where IdAssoc=$id";
        $res = PdoPPE::$monPdo->exec($req);
        

    }

 

    public function getRecupMembres($id)
    {

        $req = " select membres.* from membres,associations,contenir where contenir.IdAssoc = associations.IdAssoc and contenir.IdMembre = membres.IdMembre and contenir.IdAssoc = $id ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;




    }




    public function getLaCategorie($id)
    {
        $req = "select * from categories where IdCategories = $id";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetch();

        return $leRes;

    }



    public function GetEmailM($id){



        $req = "select Mail from membres where IdMembre = $id";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;



    }



    public function EnregistrementDemande($nomAssociation,$nomFondateur,$tel,$mail,$description,$laCategorie,$idmembre){


        $req = 'call p_SetDemande("'.$nomAssociation.'","'.$nomFondateur.'","'.$tel.'","'.$mail.'","'.$description.'","'.$laCategorie.'",'.$idmembre.')' ;
        $res = PdoPPE::$monPdo->exec($req);


    }


    public function Inscription($nom,$prenom,$ville,$cp,$adresse,$nummobile,$numfixe,$mail,$news,$contact,$login,$pass){



        $req = "select count(*) from membres";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetch();

        $id = $leRes['count(*)'] + 1 ;

        $req2 = "INSERT INTO `membres`(`IdMembre`, `NomMembre`, `Prenom`, `Ville`, `CPostal`, `Adresse`, `NoMobile`, `NoFixe`, `Mail`, `Login`, `Mdp`, `Contact`, `Newsletter`) VALUES (".$id.",'".$nom."','".$prenom."','".$ville."','".$cp."','".$adresse."','".$nummobile."','".$numfixe."','".$mail."','".$login."','".$pass."','".$contact."','".$news."')" ;
        $res = PdoPPE::$monPdo->exec($req2);

    }

    public function getInfoMembre($id){



        $req = "select * from membres where IdMembre = $id";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;



    }

    public function GetDemande(){

        $req = "select * from demande";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;

    }

    public function RefusDemande($id){


        $req2 = "Delete from demande where ID=$id " ;
        $res = PdoPPE::$monPdo->exec($req2);

    }


    public function GetRecupDemande($id){


        $req = "select * from demande where ID= $id ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;
    }
    public function GetNbrAssoc(){

        $req2 = "select count(*) from associations ";
        $res2 = PdoPPE::$monPdo->query($req2);
        $leRes2 = $res2->fetch();
        return $leRes2;


    }


    public function AjoutMembreDansAssoc($Id_M,$id){

        $req2 = "INSERT INTO `contenir`(`IdAssoc`, `IdMembre`) VALUES ($id,$Id_M)";
        $res = PdoPPE::$monPdo->exec($req2);


    }


    public function getRecupCat($categorie){
        $req4 = "select IdCategories from categories where TypeC='$categorie' ";
        $res4 = PdoPPE::$monPdo->query($req4);
        $leRes4 = $res4->fetch();


        return $leRes4;

    }
	
	

    public function SetCate($categorie){



        $req4 = "select max(IdCategories) from categories ";
        $res4 = PdoPPE::$monPdo->query($req4);
        $leRes4 = $res4->fetch();

        $id= $leRes4[0][0] + 1;

        $req2 = "INSERT INTO `categories`(`IdCategories`, `TypeC`) VALUES ($id,'$categorie')";
        $res = PdoPPE::$monPdo->exec($req2);


         RETURN $id;



    }



    public function AcceptDemande($cate,$id,$description,$mail,$tel,$nomAssociation,$Id_M){

       $req3 = "INSERT INTO `associations`(`IdAssoc`, `Nom`, `Fiche`, `Mail`, `NoTel`, `IdCategories`, `IdMembre`) VALUES ( $id,'".$nomAssociation."','".$description."','".$mail."',".$tel.",'$cate',$Id_M)" ;
        $res3 = PdoPPE::$monPdo->exec($req3);

    }


    public function modifCompteMembre($idmembre,$Nom,$Prenom,$Ville,$cp,$NoMobile,$Adresse,$NoFixe,$Mail,$Contact,$Newsletter,$Mdp,$Login){



        $req = "Update membres SET IdMembre= $idmembre , NomMembre='$Nom', Prenom='$Prenom', Ville='$Ville', CPostal='$cp', Adresse='$Adresse',NoMobile= $NoMobile, NoFixe = $NoFixe ,Mail='$Mail',Contact ='$Contact',Newsletter='$Newsletter' where IdMembre = $idmembre" ;
        $res = PdoPPE::$monPdo->exec($req);


    }


    public function getFondateurAssoc($id,$idmembre){


        $req = "select * from associations where IdMembre = $idmembre and IdAssoc = $id ";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;

    }

    public function getemail($idassoc){

        $req = "select mail from membres,contenir where contenir.IdMembre = membres.IdMembre and contenir.IdAssoc= $idassoc  and membres.Newsletter = 'oui';";
        $res = PdoPPE::$monPdo->query($req);
        $leRes = $res->fetchAll();

        return $leRes;


    }







}
