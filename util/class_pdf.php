<?php
require_once("fpdf/fpdf.php");
require_once("class.pdoPPE.inc.php");

class PDF extends FPDF
{
// Chargement des données
function LoadData($id)
{
    $pdo = PdoPPE::getPdoPPE();
    $LesMembres= $pdo-> getRecupMembres($id);

    return $LesMembres;
}






}

$pdf = new PDF();

$pdf->SetFont('Arial','B',16);
$pdf->Cell(40,10,'Membres de l\'association!');

// Titres des colonnes
$header = array('Prénom', 'Nom', 'Téléphone mobile', 'Téléphone Fixe','Adresse','Email','Contact','Newsletter');
// Chargement des données
$data = $pdf->LoadData($_REQUEST['id']);
$pdf->AddPage();

// Police Arial gras 15
    $pdf->SetFont('Arial','B',15);
    // Décalage à droite
    $pdf->Cell(100);
    // Titre
    $pdf->Cell(80,10,'Membres de l\'association',1,0,'C');
    // Saut de ligne
    $pdf->Ln(30);
	
  $pdf->SetFont('Arial','',10);
  
  
  
  foreach($data as $unM)
    {
		$pdf->Cell(40,10,utf8_decode('Prénom :'.$unM['Prenom']));
		$pdf->Ln(5);
		$pdf->Cell(40,10,utf8_decode('Nom :'.$unM['NomMembre']));
		$pdf->Ln(5);
		$pdf->Cell(40,10,'NoMobile :'.$unM['NoMobile']);
		$pdf->Ln(5);
		$pdf->Cell(40,10,'NoFixe :'.$unM['NoFixe']);
		$pdf->Ln(5);
		$pdf->Cell(40,10,utf8_decode('Adresse :'.$unM['Ville']." ".$unM['Adresse']." ".$unM['CPostal']));
		$pdf->Ln(5);
		$pdf->Cell(40,10,utf8_decode('Mail :'.$unM['Mail']));
		$pdf->Ln(5);
		$pdf->Cell(40,10,'Contact :'.$unM['Contact']);
		$pdf->Ln(5);
		$pdf->Cell(40,10,'Newsletter :'.$unM['Newsletter']);
		$pdf->Ln(15);
	
	
	
	}
  
  
		
	
	$pdf->Output();
?>