<?php


function estUnMail($mail)
{
    return  preg_match ('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#', $mail);
}


function getErreursDemande($nomAssociation,$nomFondateur,$tel,$mail,$description,$categorie)
{
    $lesErreurs = array();
    if($nomFondateur=="")
    {
        $lesErreurs[]="Il faut saisir le champ Nom du fondateur";
    }

    if($categorie=="")
    {
        $lesErreurs[]="Il faut saisir le champ catégorie";
    }
    if($nomAssociation=="")
    {
        $lesErreurs[]="Il faut saisir le champ Nom de l'association";
    }

    if($tel=="")
    {
        $lesErreurs[]="Il faut saisir le champ telephone";
    }

    if($description=="")
    {
        $lesErreurs[]="Il faut saisir le champ description";
    }

    if($mail=="")
    {
        $lesErreurs[]="Il faut saisir le champ mail";
    }

    else
    {
        if(!estUnMail($mail))
        {
            $lesErreurs[]= "erreur de mail";
        }
    }
    return $lesErreurs;
}


function getErreursInscription($nom,$prenom,$ville,$cp,$adresse,$nummobile,$numfixe,$mail,$news,$contact,$login,$pass ){

    $lesErreurs = array();
    if($nom=="")
    {
        $lesErreurs[]="Il faut saisir votre nom";
    }
    if($prenom=="")
    {
        $lesErreurs[]="Il faut saisir votre prénom";
    }

    if($ville=="")
    {
        $lesErreurs[]="Il faut saisir votre ville";
    }

    if($cp=="")
    {
        $lesErreurs[]="Il faut saisir votre code postal";
    }

    if($adresse=="")
    {
        $lesErreurs[]="Il faut saisir votre adresse";
    }

    if($nummobile=="")
    {
        $lesErreurs[]="Il faut saisir un numéro de mobile";
    }

    if($news=="")
    {
        $lesErreurs[]="Vous n'avez pas précisé votre choix sur l'inscription à la newsletter ";
    }

    if($contact=="")
    {
        $lesErreurs[]="Vous n'avez pas précisé votre choix d'être contacté ou non";
    }

    if($login=="")
    {
        $lesErreurs[]="Il faut saisir un login";
    }

    if($pass=="")
    {
        $lesErreurs[]="Il faut saisir un mot de passe";
    }

    if($mail=="")
    {
        $lesErreurs[]="Il faut saisir une adresse email";
    }
    else
    {
        if(!estUnMail($mail))
        {
            $lesErreurs[]= "erreur de mail";
        }
    }
    return $lesErreurs;


}







?>